// vite.config.ts o vite.config.js
import { defineConfig } from 'vite';

export default defineConfig({
  // ... otras configuraciones ...
  esbuild: {
    jsxFactory: 'h',
    jsxFragment: 'Fragment',
    // ... otras configuraciones ...
  },
  // ... otras configuraciones ...
});
